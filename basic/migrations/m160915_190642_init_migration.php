<?php

use yii\db\Migration;

class m160915_190642_init_migration extends Migration
{
    public function up()
    {
        $this->createTable('goods', [
            'id' => Schema::TYPE_PK,
            'img' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_TEXT,
            'description'
        ]);
    }

    public function down()
    {
        $this->dropTable('news');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
